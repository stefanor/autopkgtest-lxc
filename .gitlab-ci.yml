variables:
   WORKING_DIR: $CI_PROJECT_DIR/lxc
   ARTIFACTS_DIR: $CI_PROJECT_DIR/artifacts
   ARCH: amd64
   # arm64 runner is not enabled by default
   DISABLE_ARM64_JOBS: 1
   debci_quiet: 'false'

stages:
  - base
  - inherited

.build-base: &build-base
  stage: base
  image: registry.salsa.debian.org/salsa-ci-team/pipeline/autopkgtest
  script:
    - export
    # In case there is an underlying network already with the same network address, e.g. a runner
    # running on top of a libvirtd VM, we need to look for another network
    - |
        UNDERLYING_NET=0
        for n in $(seq 122 129); do
          bash -c "echo >/dev/tcp/192.168.${n}.1/53" \
            || { UNDERLYING_NET=${n} ; break; }
        done
        if [ "$UNDERLYING_NET" -eq 0 ]; then
          echo "unable to find an unused network" >&2
          exit 1
        fi
    - |
        cat >/etc/lxc/default.conf <<EOT
        lxc.net.0.type = veth
        lxc.net.0.link = lxcbr0
        lxc.net.0.flags = up
        lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx
        lxc.apparmor.profile = unconfined
        EOT
    - |
        cat >/etc/default/lxc-net <<EOT
        USE_LXC_BRIDGE="true"
        EOT
    - |
        cat >/etc/lxc/lxc.conf <<EOT
        lxc.lxcpath=${WORKING_DIR}
        EOT
    - /etc/init.d/lxc-net restart || true
    - /etc/init.d/lxc start
    - /etc/init.d/lxc restart
    - /etc/init.d/libvirtd start

    - test "$UNDERLYING_NET" -ne "122" && echo "The default libvirtd network seems to be unavailble and will be be replaced by 192.168.${UNDERLYING_NET}.0/24"
    - |
        if [ "$UNDERLYING_NET" -ne 122 ] ; then
          virsh net-destroy default || true
          virsh net-dumpxml default > /tmp/default.xml
          sed -i -e "s/='192\.168\.122\.\([0-9]\+\)'/='192.168.${UNDERLYING_NET}.\1'/g" /tmp/default.xml
          virsh net-undefine default
          virsh net-define /tmp/default.xml
        fi

    - virsh net-start default
    - |
        if [ ! -e /usr/share/debci/bin/debci-generate-apt-sources ]; then
            for p in $(find ${CI_PROJECT_DIR}/patches -name \*.patch | sort); do
                patch -p1 -d /usr/share/debci <$p;
            done
        fi
    - debci setup --suite ${CI_JOB_NAME%%_*}

  after_script:
    - mkdir -p ${ARTIFACTS_DIR}
    - tar -cf ${ARTIFACTS_DIR}/lxc-${ARCH}.tar --exclude /dev -C ${WORKING_DIR} .
    - md5sum ${ARTIFACTS_DIR}/lxc-${ARCH}.tar > ${ARTIFACTS_DIR}/lxc-${ARCH}.tar.md5

  artifacts:
    paths:
    - ${ARTIFACTS_DIR}
  retry:
    max: 1
    when:
      - script_failure

.build-inherited: &build-inherited
  stage: inherited
  image: debian
  # dependencies:
  #   - '<base-suite>'
  script:
    - mkdir -p "${WORKING_DIR}"
    - tar -C "${WORKING_DIR}" -xf "${ARTIFACTS_DIR}/lxc-${ARCH}.tar"
    - BASENAME=$(ls -1 "${WORKING_DIR}" | sed -e 's/^autopkgtest-//' -e "s/-${ARCH}"'$'"//")
    - mv "${WORKING_DIR}"/autopkgtest-${BASENAME}-${ARCH} "${WORKING_DIR}/autopkgtest-${CI_JOB_NAME%%_*}-${ARCH}"
    - echo "deb http://deb.debian.org/debian ${CI_JOB_NAME%%_*} main" > "${WORKING_DIR}/autopkgtest-${CI_JOB_NAME%%_*}-${ARCH}/rootfs/etc/apt/sources.list.d/sci.list"
    - sed -i -e "s/${BASENAME}/${CI_JOB_NAME%%_*}/g" -e '/#lxc\.include.*nesting/ s/^#//' "${WORKING_DIR}/autopkgtest-${CI_JOB_NAME%%_*}-${ARCH}/config"
    - tar -cf "${ARTIFACTS_DIR}/lxc-${ARCH}.tar" --exclude /dev -C "${WORKING_DIR}" .
    - md5sum ${ARTIFACTS_DIR}/lxc-${ARCH}.tar > ${ARTIFACTS_DIR}/lxc-${ARCH}.tar.md5
  artifacts:
    paths:
    - ${ARTIFACTS_DIR}

# debootstrap doesn't have a script for experimental
# we need to make this job depend on unstable
experimental:
  extends: .build-inherited
  dependencies:
    - unstable

unstable:
  extends: .build-base

unstable_arm64:
  extends: .build-base
  #image: registry.salsa.debian.org/josch/pipeline/arm64v8/autopkgtest:sid_arm64images
  image: registry.salsa.debian.org/salsa-ci-team/pipeline/arm64v8/autopkgtest:sid
  variables:
    ARCH: arm64
  tags:
    - arm64
  rules:
    - if: $CI_PROJECT_PATH == "salsa-ci-team/autopkgtest-lxc"
    - if: $DISABLE_ARM64_JOBS !~ /^(1|yes|true)$/

testing:
  extends: .build-base

stable:
  extends: .build-base

stable-backports:
  extends: .build-inherited
  dependencies:
    - stable

oldstable:
  extends: .build-base

oldstable-backports:
  extends: .build-inherited
  dependencies:
    - oldstable

oldoldstable:
  extends: .build-base

rc-buggy:
  extends: .build-inherited
  dependencies:
    - unstable

sid:
  extends: .build-inherited
  dependencies:
    - unstable

sid_arm64:
  extends: .build-inherited
  variables:
    ARCH: arm64
  dependencies:
    - unstable_arm64
  rules:
    - if: $CI_PROJECT_PATH == "salsa-ci-team/autopkgtest-lxc"
    - if: $DISABLE_ARM64_JOBS !~ /^(1|yes|true)$/

trixie:
  extends: .build-inherited
  dependencies:
    - testing

bookworm:
  extends: .build-inherited
  dependencies:
    - stable

bookworm-backports:
  extends: .build-inherited
  dependencies:
    - stable

bullseye:
  extends: .build-inherited
  dependencies:
    - oldstable

bullseye-backports:
  extends: .build-inherited
  dependencies:
    - oldstable

buster:
  extends: .build-inherited
  dependencies:
    - oldoldstable

buster-backports:
  extends: .build-inherited
  dependencies:
    - oldoldstable
