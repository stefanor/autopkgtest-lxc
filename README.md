# Autopkgtest LXC containers

Build LXC containers for Salsa CI autopkgtest job.

## ARM64 jobs

At the moment, there is not a global ARM shared runner available.
Therefore, the arm64 jobs in this pipeline are disabled by
default.

However, there is a ARM runner (managed by the Salsa CI Team) that 
you can enable in your `autopkgtest-lxc` fork in salsa.d.o. For that,
go to Settings ->  CI/CD -> Runners -> Project
Runners -> salsaci-arm64-runner-01.debian.net -> Enable for this project.

Then enable the arm64 jobs in the pipeline by running it manually, and setting
`DISABLE_ARM64_JOB` to anything different than 1, 'yes' or 'true'.
